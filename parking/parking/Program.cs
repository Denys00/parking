﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parking
{
    class Program
    {
        private static async Task GetFeeAsync()
        {
            var park = Parking.Initilize();
            while (!ParkingConf.Done)
            {
                await Task.Delay(ParkingConf.TransactionPeriod * 1000);
                park.GetFee();
            }
        }

        private static async Task LogTransactionsAsync()
        {
            var park = Parking.Initilize();
            while (!ParkingConf.Done)
            {
                await Task.Delay(ParkingConf.LogPeriod * 1000);
                park.LogTransactions();
            }
        }

        private static void ClearConsole()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Console.Clear();
        }

        private static Car CreateCar()
        {
            Console.WriteLine("Input: Car Name | Car Type | Car Balance");
            var lines = Console.ReadLine().Split();
            if (lines.Length != 3) return null;
            CarType type;
            switch (lines[1].ToLower())
            {
                case "light":
                    type = CarType.Light;
                    break;
                case "bus":
                    type = CarType.Bus;
                    break;
                case "motocycle":
                    type = CarType.Motocycle;
                    break;
                case "cargo":
                    type = CarType.Cargo;
                    break;
                default:
                    return null;
            }
            decimal balance;
            try
            {
                balance = Convert.ToDecimal(lines[2]);
            }
            catch(Exception e)
            {
                return null;
            }

            return new Car(lines[0], type, balance);

        }

        private static int ChooseCar(Parking park)
        {
            Console.WriteLine("Select Id Of Car");
            park.AllTransport();
            int selection;
            try
            {
                selection = Convert.ToInt32(Console.ReadLine());
                if (selection < 0 || selection >= park.CountOfCars())
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Wrong Input");
                return -1;
            }
            return selection;
        }

        private static async Task ShowMenuAsync()
        {
            var park = Parking.Initilize();
            while (!ParkingConf.Done)
            {
                int answer = MenuOutput();
                switch (answer)
                {
                    case 1:
                        Console.WriteLine($"Current balance is: {park.Balance}");
                        ClearConsole();
                        break;
                    case 2:
                        Console.WriteLine($"Money for last minute: {park.MoneyForLastMinute()}");
                        ClearConsole();
                        break;
                    case 3:
                        park.CountOfPlaces();
                        ClearConsole();
                        break;
                    case 4:
                        park.LastTransactions();
                        ClearConsole();
                        break;
                    case 5:
                        park.AllTransactions();
                        ClearConsole();
                        break;
                    case 6:
                        park.AllTransport();
                        ClearConsole();
                        break;
                    case 7:
                        Car car = CreateCar();
                        if (car != null)
                        {
                            park.AddCar(car);
                        }
                        ClearConsole();
                        break;
                    case 8:
                        int select = ChooseCar(park);
                        if(select != -1)
                        {
                            park.DeleteCar(select);
                        }
                        ClearConsole();
                        break;
                    case 9:
                        int choose = ChooseCar(park);
                        if(choose != -1)
                        {
                            Console.WriteLine("Input Sum: ");
                            try
                            {
                                decimal sum = Convert.ToDecimal(Console.ReadLine());
                                park.RefillCarBalance(choose, sum);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Wrong input");
                            }
                        }
                        ClearConsole();
                        break;
                    case 10:
                        ParkingConf.SetDone();
                        break;
                    default:
                        Console.WriteLine("Wrong Input");
                        break;
                }
            }
        }

        private static async Task RunTelepromter()
        {
            var startGettingFee = GetFeeAsync();
            var logTransactions = LogTransactionsAsync();
            var menu = ShowMenuAsync();

            await Task.WhenAny(startGettingFee, logTransactions, menu);
        }

        private static int MenuOutput()
        {
            Console.WriteLine("============ Menu =================");
            Console.WriteLine("1 - See Curent Balance Of Parking");
            Console.WriteLine("2 - Money Earned For Last Minute");
            Console.WriteLine("3 - Count Of Free/Busy Places");
            Console.WriteLine("4 - Print Transactions Of Last Minute");
            Console.WriteLine("5 - Print All Transactions");
            Console.WriteLine("6 - Print All Transport");
            Console.WriteLine("7 - Put Transport In Parking");
            Console.WriteLine("8 - Take Transport From Parking");
            Console.WriteLine("9 - Refill Car Balance");
            Console.WriteLine("10 - Close");
            string answer = Console.ReadLine();
            return Convert.ToInt32(answer);
        }

        static void Main(string[] args)
        {
            string path = @"C:\MyProjects\binary-studio\parking\parking\parking\Transactions.log";

            Parking park = Parking.Initilize();
            park.AddLogPath(path);
            park.AddCar(new Car("Audi222", CarType.Light, 100));
            park.AddCar(new Car("Electron", CarType.Bus, 100));
            park.AddCar(new Car("Viper", CarType.Motocycle, 100));
            park.RefillCarBalance(1, -100);
            RunTelepromter().Wait();
            
        }
    }
}
